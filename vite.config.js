import { fileURLToPath, URL } from "node:url";

import { defineConfig, preview } from "vite";
import { quasar, transformAssetUrls } from "@quasar/vite-plugin";
import vue from "@vitejs/plugin-vue";
// import federation from "@originjs/vite-plugin-federation";
import * as dotenv from "dotenv";

dotenv.config();

// const UTILS = process.env.UTILS
// const HOST = process.env.HOST

// https://vitejs.dev/config/
export default defineConfig({
    server: {
        port: process.env.PORT,
        strictPort: process.env.PORT,
    },
    preview: {
        port: process.env.PORT,
        strictPort: process.env.PORT,
    },
    plugins: [
        vue({
            template: { transformAssetUrls },
        }),
        // Config for Remote
        // federation({
        //     name: "remote_app",
        //     filename: "remoteEntry.js",
        //     exposes: {
        //         "./User": "./src/views/Users/Index.vue",
        //         "./user_store": "./src/stores/user.js",
        //     },
        //     shared: ["vue", "pinia"],
        // }),

        // Config for Host
        // federation({
        //     name: "app",
        //     remotes: {
        //         remoteApp: `${HOST}/${UTILS}/assets/remoteEntry.js`,
        //     },
        //     shared: ["vue", "pinia"],
        // }),
        quasar({
            sassVariables: "src/assets/css/quasar-variables.sass",
        }),
    ],
    resolve: {
        alias: {
            "@": fileURLToPath(new URL("./src", import.meta.url)),
            src: fileURLToPath(new URL("./src", import.meta.url)),
            components: fileURLToPath(
                new URL("./src/components", import.meta.url)
            ),
            layouts: fileURLToPath(new URL("./src/layouts", import.meta.url)),
            pages: fileURLToPath(new URL("./src/pages", import.meta.url)),
            assets: fileURLToPath(new URL("./src/assets", import.meta.url)),
            stores: fileURLToPath(new URL("./src/stores", import.meta.url)),
        },
    },
    build: {
        modulePreload: false,
        target: "esnext",
        minify: true,
        cssCodeSplit: false,
    },
});
