import axios from "axios";

const api = axios.create({
    baseURL: import.meta.env.VITE_API_URL,
    timeout: 30000,
});

api.interceptors.request.use((config) => {
    config.headers = {
        Authorization: "",
        "Content-Type": "application/json",
        Accept: "application/json",
    };

    return config;
});

api.interceptors.response.use(
    (response) => response,
    (error) => {
        if (error.response.status == 401) {
            // 401 Unauthorized
            // Do Something..
        } else if (error.response.status == 400) {
            // 400 Bad Request
            // Do Something..
        } else if (error.response.status == 500) {
            // 500 Internal Server Error
            // Do Something..
        }
    }
);

export default api;
