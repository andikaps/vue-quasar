import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../pages/HomeView.vue";

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: "/",
            component: () => import("../layouts/MainLayout.vue"),
            children: [
                {
                    path: "",
                    name: "home",
                    component: HomeView,
                },
            ],
        },
    ],
});

export default router;
